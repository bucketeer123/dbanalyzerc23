package application;
import java.sql.*;


public class TestConnection {
	
	// Tests for a connection to the database
	public boolean verifyConnection() {
		boolean connection_status = false;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://10.0.75.1:3307/db_grad","root","ppp");  
			//here sonoo is database name, root is username and password  
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from users");
	      	if(rs.next()) {

	      		connection_status = true;
		    }
	      	else {
	      		connection_status = false;
	      	}
			
		}
		catch(Exception e) {
			connection_status = false;
		}	
		return connection_status;
	}	
}
