/**
 * 
 */
package application;
import java.sql.*;

/**
 * @author Graduate
 *
 */
/*public class VerifyLogin {
	public String validateLogin(String username, String password) {
		String result = "";
	      try{      
	          Class.forName("com.mysql.jdbc.Driver");  // MySQL database connection
	          Connection conn = DriverManager.getConnection("jdbc:mysql://10.0.75.1:3307/db_grad","root","ppp");    
	          Statement pst = conn.createStatement();
	          String q = "Select user_id, user_pwd FROM db_grad.users WHERE user_id = '" + username + "' AND user_pwd = '" + password + "';";
	          ResultSet rs = pst.executeQuery(q);    
	          if(rs.next())           
	        	  result = "Valid login credentials! Welcome " + username + ".";
	          else
	        	 result = "Invalid login credentials, please try again.";           
	     }
	     catch(Exception e){       
	    	 result = "Something went wrong !! Please try again";       
	     } 
		return result;
	}
}*/

public class VerifyLogin {
	public String validateLogin(String username, String password) {
		String result = "";
		
		
	      try{      
	          Class.forName("com.mysql.jdbc.Driver");  // MySQL database connection
	          Connection conn = DriverManager.getConnection("jdbc:mysql://10.0.75.1:3307/db_grad","root","ppp");    
	          
	          PreparedStatement stmt = conn.prepareStatement("Select user_id, user_pwd FROM db_grad.users WHERE user_id = ? AND user_pwd = ?");
	          stmt.setString(1, username);
	          stmt.setString(2, password);
	          ResultSet rs = stmt.executeQuery();
	          if(rs.next())           
	        	  result = "Valid login credentials!";    
	          
	          else
	        	 result = "Invalid login credentials, please try again.";           
	     }
	     catch(Exception e){       
	    	 result = "Something went wrong !! Please try again";       
	     } 
		return result;
	}
}
