<%-- 
	Document: Login
	Author(s): Austin and Nishal
	Description: The login page for our application. This will verify that there is a successful connection to the
	database as well as allow the user to login.
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>DB Investment Services Analyzer</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/example/resources/style.css">
	<link rel="icon" href="/example/resources/index.ico" type="image/x-icon" />
</head>
<body>
	<jsp:useBean id="connectionUtils"
		class="application.ShowConnectionStatus" />
	<jsp:useBean id="connectionCheck" class="application.TestConnection" />


	<%-- <%= connectionUtils.returnStatus(true) %> --%>
	<!-- 	This is to run a single line of java. -->

<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" style="cursor: default;" href="#">
    <img src="/example/resources/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
    Deutsche Bank
  </a>
  <span class="navbar-text">
	<%
		boolean answer = connectionCheck.verifyConnection();
		out.println(connectionUtils.returnStatus(answer));
	%>
    </span>
</nav>

	<div class="Login" id="login_box" >
		<div class="container">
			<div class="Absolute-Center">
			 <div class="container" style="background-color:white; border-radius:8px; padding-bottom:20px;padding-top:20px;padding-left:20px;padding-right:20px;width:400px">
				<h4 style="text-align: center;color: black">Investment Services Analyzer</h4>
				<div class="input-group mb-3">
					<span class="input-group-text"><i class="fas fa-user"></i></span> <input class="form-control"
						type="text" name="username" id="usrname" placeholder="Username"
						required />
				</div>
				<div class="input-group mb-3">
					<span class="input-group-text"><i class="fas fa-lock"></i></span> <input class="form-control"
						type="password" name="password" id="pwd" placeholder="Password"
						required />
					<!-- 	Login: <input type="submit" value="Login" id="call" /> -->
				</div>
				<button class="btn btn-def btn-block" id="login_button" onclick="loadDoc()">Login</button>
				<div id="mobiles" style="color:black"></div>
				</div>
			</div>
		</div>
	</div>

	<div id="demo">
	</div>


	<script>
		//This makes sure that when you press enter it runs clicks the login button
		var input_1 = document.getElementById("pwd");
		input_1.addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				document.getElementById("login_button").click();
			}
		});
		
		var input_2 = document.getElementById("usrname");
		input_2.addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				document.getElementById("login_button").click();
			}
		});
	</script>


	<script>
		//Ensures that the username and password criteria are valid.
		function loadDoc() {
			var xhttp = new XMLHttpRequest();
			var username = document.getElementById("usrname").value;
			var password = document.getElementById("pwd").value;
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(this.response.includes("Valid login credentials!")){
						document.querySelector('#login_box').hidden = true;
						document.querySelector('#mobiles').hidden = true;
						
						
						//Example template of how to create a div on successful login.
						var div = document.createElement("div");
						div.style.width = "100px";
						div.style.height = "100px";
						div.style.background = "red";
						div.style.color = "white";
						div.innerHTML = "Hello";

						document.getElementById("demo").appendChild(div);
						
						
						
						
						
					} else {
						document.getElementById("mobiles").innerHTML = this.response;
					}
				}
			};
			xhttp.open("POST", "validate.jsp", true);
			xhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			xhttp.send("usrnm=" + username + "&passwrd=" + password);
		}
	</script>


</body>
</html>
