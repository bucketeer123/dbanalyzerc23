<%-- 
	Document: Validation Form
	Author(s): Austin
	Description: This will be called after posting the form from the login page. The security of
	this specific module must be built upon after multiple forms of pen-testing
--%>

<%@ page import ="java.sql.*" %>
<jsp:useBean id="vrfyLogin" class="application.VerifyLogin" />

<%
	String verify = vrfyLogin.validateLogin(request.getParameter("usrnm"), request.getParameter("passwrd"));
	out.println(verify);

%>

